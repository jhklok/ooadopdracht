package nl.han.ica;



import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

public class DeelnemenToetsController  {
    private ToetsDeelname toetsDeelname;
    private boolean deelnemen = true;


    public DeelnemenToetsController(ToetsDeelname toetsDeelname) {
        this.toetsDeelname = toetsDeelname;

    }




    public void BeantwoordVraag(String antwoord) {
        toetsDeelname.beantwoordVraag(antwoord);
    }

    public void startToets() {
        toetsDeelname.startToets();
    }

    public void stopToets() {
        deelnemen = false;
        toetsDeelname.stopToets();
    }

    public void wijzigAntwoord(Scanner scanner) {
        toetsDeelname.toonBeantwoordeVragen();
        boolean vraagGekozen = false;
        while (!vraagGekozen) {
            if(scanner.hasNextInt()) {
                kiesVraag(scanner.nextInt());
                vraagGekozen = true;
            }
        }

    }

    public boolean kiesVraag(int vraagNummer) {
        return toetsDeelname.kiesVraag(vraagNummer);
    }





   public void run() {
        System.out.println("Druk op S om de toets te starten");
       System.out.println("Druk op V om een antwoord te wijzigen");
       System.out.println("Druk op E om de deelname te stoppen");

       Scanner scanner = new Scanner(System.in);
       while(deelnemen) {
           String scanString = scanner.nextLine();
           if(toetsDeelname.isGesloten()) {
               stopToets();
           }
           else if(scanString.equals("S")) {
               startToets();
           }
          else if(scanString.equals("V")) {
               wijzigAntwoord(scanner);
           }
           else if(scanString.equals("E")) {
               stopToets();
           }
           else {
               BeantwoordVraag(scanString);
           }
       }
       System.out.println("Deelname gesloten");
   }


}
