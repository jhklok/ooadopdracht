package nl.han.ica;

public class GeslotenAntwoord extends Toetsantwoord {
    private int geselecteerdeOptie;
    private Optie juisteOptie;

    public GeslotenAntwoord(int geselecteerdeOptie, Optie juisteOptie) {
        this.geselecteerdeOptie = geselecteerdeOptie;
        this.juisteOptie = juisteOptie;
    }

    @Override
    public void toonAntwoord() {
        System.out.println(geselecteerdeOptie);
    }
}
