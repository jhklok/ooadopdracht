package nl.han.ica;

import java.util.ArrayList;

public class GeslotenVraag extends Vraag {
    private ArrayList<Optie> opties;

    public GeslotenVraag(String vraag, ArrayList<Optie> opties) {
        super(vraag);
        this.opties = opties;
    }

    private Optie getAntwoordOptie(int antwoord) {
        for (Optie o:opties) {
            if(o.getSelectieNummer() == antwoord) {
                return o;
            }
        }
        return null;
    }

    @Override
    Toetsantwoord beantwoord(String antwoord) {
        int selectieNummer;
        try {
             selectieNummer = Integer.parseInt(antwoord);
            return new GeslotenAntwoord(selectieNummer, getAntwoordOptie(selectieNummer));

        } catch (Exception e) {
         System.out.println("Selecteer een beschikbare optie");
         return null;
        }
    }

    @Override
    public void toonVraag() {
        System.out.println(vraag);
        for(Optie o:opties) {
            o.toonOptie();
        }
    }


}
