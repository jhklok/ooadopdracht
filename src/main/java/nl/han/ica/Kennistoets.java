package nl.han.ica;

import java.util.ArrayList;

public class Kennistoets {
    private String naam;
    private ArrayList<VraagInToets> vragenInToets;

    public Kennistoets(String naam, ArrayList<VraagInToets> vragenInToets) {
        this.naam = naam;
        this.vragenInToets = vragenInToets;
    }

    public ArrayList<VraagInToets> getVragenInToets() {
        return vragenInToets;
    }

}
