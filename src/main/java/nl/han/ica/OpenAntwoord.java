package nl.han.ica;

import java.util.ArrayList;

public class OpenAntwoord extends Toetsantwoord {
    private String antwoord;
    private ArrayList<MogelijkAntwoord> mogelijkAntwoorden;

    public OpenAntwoord(String antwoord, ArrayList<MogelijkAntwoord> mogelijkAntwoorden) {
        this.antwoord = antwoord;
        this.mogelijkAntwoorden = mogelijkAntwoorden;
    }

    @Override
    public void toonAntwoord() {
        System.out.println(antwoord);
    }
}
