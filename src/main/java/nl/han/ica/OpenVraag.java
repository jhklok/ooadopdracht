package nl.han.ica;

import java.util.ArrayList;

public class OpenVraag extends Vraag {
    ArrayList<MogelijkAntwoord> mogelijkAntwoorden;

    public OpenVraag(String vraag,ArrayList<MogelijkAntwoord> mogelijkAntwoorden) {
        super(vraag);
        this.mogelijkAntwoorden = mogelijkAntwoorden;
    }

    @Override
    Toetsantwoord beantwoord(String antwoord) {
        return new OpenAntwoord(antwoord, mogelijkAntwoorden);
    }

    @Override
    public void toonVraag() {
        System.out.println(vraag);
    }
}
