package nl.han.ica;

public class Optie {
    private String antwoordTekst;
    private boolean goedFout;
    private int selectieNummer;

    public Optie(String antwoordTekst, boolean goedFout, int selectieNummer) {
        this.antwoordTekst = antwoordTekst;
        this.goedFout = goedFout;
        this.selectieNummer = selectieNummer;
    }


    public int getSelectieNummer() {
        return selectieNummer;
    }


    public boolean isjuist() {
        return goedFout;
    }

    public void toonOptie() {
        System.out.println(selectieNummer + " = " + antwoordTekst);
    }
}
