package nl.han.ica;

import org.apache.commons.lang3.time.StopWatch;

import java.util.ArrayList;
import java.util.Date;


public class ToetsDeelname  {
    private Date startDatum;
    private long Deelnametijd;
    private ArrayList<VraagInToets> ToetsVragen;
    private StopWatch stopWatch;
    private int huidigeVraag;
    private boolean gesloten = false;


    public ToetsDeelname(ArrayList<VraagInToets> toetsVragen) {
        this.ToetsVragen = toetsVragen;
        this.huidigeVraag = 1;
        this.Deelnametijd = 0;
        this.startDatum = new Date();
    }


    private int bepaalHuidigeVraag() {
        int huidigeVraag = 0;
        for (VraagInToets v:ToetsVragen) {
            if(!v.isBeantwoord()) {
                huidigeVraag = v.getVolgnummer();
            }
        }
        return huidigeVraag;
    }

    private VraagInToets getVraagInToets(int volgNummer) throws Exception {
        for(VraagInToets v: ToetsVragen) {
            if(v.getVolgnummer() == volgNummer) {
                return v;
            }
        }
        throw new  Exception();
    }


    private void toonHuidigeVraag() {
        try {
            getVraagInToets(huidigeVraag).toonVraag();
        } catch (Exception e) {
            System.out.println("Geen vragen meer gevonden, druk op S om de toets te stoppen");
        }
    }

    private boolean vraagInToets(int vraagnummer) {
        boolean inToets = false;
        for(VraagInToets vraagInToets: ToetsVragen) {
            if(vraagInToets.getVolgnummer() == vraagnummer) {
                inToets = true;
            }
        }
        return inToets;
    }


    public void beantwoordVraag(String antwoord) {

            VraagInToets vraagInToets;
            try {
                vraagInToets = getVraagInToets(huidigeVraag);
                vraagInToets.beantwoord(antwoord);

            } catch (Exception e) {
                System.out.println("Vraag kon niet gevonden worden");
            }
            if (vraagInToets(bepaalHuidigeVraag())) {
                huidigeVraag = bepaalHuidigeVraag();
                toonHuidigeVraag();
            } else {
                System.out.println("Alle vragen zijn beantwoord");
            }



    }

    public void startToets() {
        stopWatch = StopWatch.createStarted();
        toonHuidigeVraag();

    }

    public void stopToets() {
        if(stopWatch != null && !stopWatch.isStopped()) {
                System.out.println("Deelname wordt gesloten");
                stopWatch.stop();
                Deelnametijd = stopWatch.getTime();
        }
        gesloten = true;

    }

    public boolean isGesloten() {
        return gesloten;
    }


    public boolean kiesVraag(int vraagNummer) {

        if(vraagInToets(vraagNummer)) {
            huidigeVraag = vraagNummer;
            toonHuidigeVraag();
            return true;
        }
        else {
            System.out.println("Deze vraag zit niet in de toets");
            return false;
        }
    }

    public void toonBeantwoordeVragen() {
        for(VraagInToets vraagInToets:ToetsVragen) {
            if(vraagInToets.isBeantwoord()) {
                System.out.println("Vraagnummer = " + vraagInToets.getVolgnummer());
                vraagInToets.toonVraagEnAntwoord();
            }
        }
    }



}
