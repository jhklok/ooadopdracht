package nl.han.ica;

import org.apache.commons.lang3.time.StopWatch;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Observable;

public class Toetsuitvoer  implements Runnable {
    private Date datum;
    private int uitvoerTijd;
    private boolean isInUitvoer;
    private Kennistoets kennistoets;
    private ArrayList<ToetsDeelname> toetsDeelnames;
    private StopWatch stopWatch;

    public Toetsuitvoer(Kennistoets kennistoets, int uitvoerTijd) {
        isInUitvoer = false;
        this.kennistoets = kennistoets;
        this.toetsDeelnames = new ArrayList<>();
        this.uitvoerTijd = uitvoerTijd;
        startuitvoer();
        datum = new Date();
    }

    @Override
    public void run() {
        while (isInUitvoer) {
            if (stopWatch.getTime() >= uitvoerTijd) {
                stopWatch.stop();
                isInUitvoer = false;
                for(ToetsDeelname deelname:toetsDeelnames ) {
                    deelname.stopToets();
                }
            }
        }
    }

    public void startuitvoer() {
        stopWatch = StopWatch.createStarted();
        isInUitvoer = true;
        new Thread(this).start();
    }

    private ToetsDeelname maakToetsDeelname() {
        ToetsDeelname toetsDeelname = new ToetsDeelname(kennistoets.getVragenInToets());
        toetsDeelnames.add(toetsDeelname);
        return toetsDeelname;
    }

    public DeelnemenToetsController maakToetsController() {
        DeelnemenToetsController deelnemenToetsController = new DeelnemenToetsController(maakToetsDeelname());
        return deelnemenToetsController;
    }
}
