package nl.han.ica;

public abstract class Vraag {
     protected String vraag;

     public Vraag(String vraag) {
          this.vraag = vraag;
     }

     abstract Toetsantwoord beantwoord(String antwoord);

     abstract void toonVraag();

}
