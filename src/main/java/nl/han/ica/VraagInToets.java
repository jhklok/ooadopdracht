package nl.han.ica;

public class VraagInToets {
    private int volgnummer;
    private Vraag vraag;
    private Toetsantwoord toetsantwoord;

    public VraagInToets(int volgnummer, Vraag vraag) {
        this.volgnummer = volgnummer;
        this.vraag = vraag;
    }

    public boolean isBeantwoord() {
        return toetsantwoord != null;
    }

    public int getVolgnummer() {
        return volgnummer;
    }

    public void beantwoord(String antwoord) {
        toetsantwoord = vraag.beantwoord(antwoord);
    }

    public void toonVraag() {
        vraag.toonVraag();
    }

    public void toonVraagEnAntwoord() {
        vraag.toonVraag();
        toetsantwoord.toonAntwoord();
    }
}
