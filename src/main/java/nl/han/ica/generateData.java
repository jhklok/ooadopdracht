package nl.han.ica;

import java.util.ArrayList;

public class generateData {


    private Vraag generateOpenVraag() {
        ArrayList<MogelijkAntwoord> mogelijkeAntwoorden = new ArrayList<>();
        mogelijkeAntwoorden.add(new MogelijkAntwoord("Kat"));
        mogelijkeAntwoorden.add(new MogelijkAntwoord("Hond"));
        return new OpenVraag("geef een viervoetig huisdier", mogelijkeAntwoorden);
    }

    private Vraag generateGeslotenVraag() {
        ArrayList<Optie> opties = new ArrayList<>();
        opties.add(new Optie("0",false, 1));
        opties.add(new Optie("1",false, 2));
        opties.add(new Optie("2",true, 3));
        return new GeslotenVraag("Hoeveel is 1 + 1?", opties);
    }

    private Kennistoets generateKennistoets() {
        ArrayList<VraagInToets> vragenInToets = new ArrayList<>();
        vragenInToets.add(new VraagInToets(1, generateOpenVraag()));
        vragenInToets.add(new VraagInToets(2, generateGeslotenVraag()));
        return new Kennistoets( "TestToets", vragenInToets);
    }

    private Toetsuitvoer generateToetsuitvoer() {
        return new Toetsuitvoer(generateKennistoets(), 100000);
    }


    public DeelnemenToetsController generateDeelnemenToetsController() {
        return generateToetsuitvoer().maakToetsController();
    }



}
